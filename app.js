Ext.require('Ext.device.Notification');
Ext.require('Ext.device.Camera');
Ext.require('Ext.device.Capture');
Ext.require('Ext.device.filesystem.Cordova');

Ext.application({
    name: 'ProductManager',

    requires: [
        'Ext.MessageBox'
    ],

    views: [
        'Main'
    ],
    
    stores: ['ProductManager.store.Config'],

    serverUrl: null,

    controllers: ['Product', 'Category', 'Config'],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        Ext.fly('appLoadingIndicator').destroy();

        //Create main view
        Ext.Viewport.add(Ext.create('ProductManager.view.Main'));
    },

    getServerUrl: function() {
        if (this.serverUrl) return this.serverUrl;
        
        var me = this,
            defaultUrl = "http://192.168.1.100/services",
            configStore = Ext.create('ProductManager.store.Config'),
            url = null;
        
        configStore.load({
            callback: function(records, operation, success) {
                if (success) {
                    if (records.length > 0) {
                        url = records[0].data.server;

                        me.setServerUrl(url);
                    } else {
                        configStore.add({ server: defaultUrl });

                        configStore.sync();

                        url = defaultUrl;
                    }

                    return url;

                } else {
                    Ext.device.Notification.show({
                        title: 'Error',
                        message: "There was a problem loading App Config Data. You might have to reload the app.",
                        buttons: Ext.MessageBox.OK
                    });

                    return defaultUrl;
                }
            },
            scope: this
        });

        return url;
    },

    setServerUrl: function(newUrl) {
        this.serverUrl = newUrl;
    }
});
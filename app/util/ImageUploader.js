Ext.define('ProductManager.util.ImageUploader', {
    upload: function (fileUri, url, extraParams, success, failure) {
        if (!fileUri) throw new Error("File uri is mandatory. You must specify one.");

        if (Ext.browser.is.PhoneGap) {
            var options = new FileUploadOptions();

            options.fileKey = "file";

            options.params = extraParams;

            var fileTransfer = new FileTransfer();
            fileTransfer.upload(fileUri, encodeURI(url), success, failure, options);
        } else {
            throw new Error("You must compile this app with phonegap for this to work.");
        }
    }
})
Ext.define('ProductManager.model.Category', {
    extend: 'Ext.data.Model',

    requires: ['ProductManager.model.Product'],

    config: {
        fields: [
            { name: 'id', type: 'string', mapping: 'Id' },
            { name: 'CategoryId', type: 'string', mapping: 'Id' },
            { name: 'ImageId', type: 'int' },
            { name: 'ParentCategoryId', type: 'string' },
            { name: 'Image', type: 'string' },
            { name: 'Name', type: 'string' },
            { name: 'RowVersion', type: 'string' },
            { name: 'Description', type: 'string' },
            { name: 'leaf', type: 'boolean', mapping: 'ChildrenType' },
            { name: 'ChildrenType', type: 'int' },
            { name: 'HasSubCategories', type: 'boolean', defaultValue: false },
            { name: 'HasProducts', type: 'boolean', defaultValue: false },
            {
                name: 'url',
                convert: function (v, record) {
                    return ProductManager.app.getServerUrl().concat("/api/CategoryImages?id=", record.data.CategoryId);
                }
            }
        ],

        validations: [
            { type: 'presence', field: 'Name' },
            { type: 'presence', field: 'Description' }
        ],

        proxy: {
            type: 'rest',
            url: ProductManager.app.getServerUrl() + '/api/categories/',
            pageParam: false,
            startParam: false,
            limitParam: false
        },

        associations: [{
            type: 'hasMany',
            model: 'ProductManager.model.Product',
            name: 'products',
            store: {
                pageSize: 50,
                clearOnLoadPage: false
            }
        }]
    }
});
﻿Ext.define('ProductManager.model.Config', {
    extend: 'Ext.data.Model',
    config: {
        fields: ['server'],

        identifier: {
            type: 'uuid'
        },

        proxy: {
            type: 'localstorage',
            id: 'configuration-data'
        }
    }
});
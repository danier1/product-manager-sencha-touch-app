Ext.define('ProductManager.model.Product', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            { name: 'id', type: 'string', mapping: 'Id' },
            { name: 'ProductId', type: 'string', mapping: 'Id' },
            { name: 'CategoryId', type: 'string' },
            { name: 'Name', type: 'string' },
            { name: 'Description', type: 'string' },
            { name: 'ImageId', type: 'string' },
            { name: 'Price', type: 'float' },
            { name: 'Quantity', type: 'float' },
            { name: 'RowVersion', type: 'string' },
            {
                name: 'url',
                convert: function (v, record) {
                    return ProductManager.app.getServerUrl().concat("/api/ProductImages?id=", record.data.ProductId);
                }
            }
        ],

        validations: [
            { type: 'presence', field: 'Name' },
            { type: 'presence', field: 'Description' },
            { type: 'format', field: 'Price', matcher: /\d+(\.\d+)?/ },
            { type: 'format', field: 'Quantity', matcher: /\d+?/ }
        ],

        proxy: {
            type: 'rest',
            url: ProductManager.app.getServerUrl() + '/api/products/',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    }
});
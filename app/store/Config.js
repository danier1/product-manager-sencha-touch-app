﻿Ext.define('ProductManager.store.Config', {
    extend: 'Ext.data.Store',
    
    id: 'Config',
    
    requires: ['ProductManager.model.Config'],

    config: {
        model: 'ProductManager.model.Config'
    }
});
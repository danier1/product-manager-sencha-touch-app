Ext.define('ProductManager.store.Categories', {
    extend: 'Ext.data.TreeStore',
    requires: ['ProductManager.model.Category'],

    config: {
        nodeParam: 'parentCategoryId',
        model: 'ProductManager.model.Category',
        root: {
            expanded: true
        }
    }
});
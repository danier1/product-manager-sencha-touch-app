Ext.define('ProductManager.view.ProductDetail', {
    extend: 'Ext.Container',

    xtype: 'productdetail',

    config: {
        baseCls: 'product-detail',

        tpl: new Ext.XTemplate(
            '<tpl if="ProductId">',
                '<img class="image" src="{url}"></img>',
                '<div class="title">Info</div>',
                '<div class="info">',
                    '<h1>Name:</h1> <span>{Name}</span>',
                    '<h1>Price:</h1><span> {Price}</span>',
                    '<h1>Quantity:</h1><span> {Quantity}</span>',
                    '<h1>Description:</h1><span class="description"> {Description}</span>',
                    '<div class="clear"></div>',
                '</div>',
                '<div class="page-counter">Page {CurrentPage} of {TotalPages}</div>',

            '<tpl else>',
                '<div class="no-items">',
                    '<div class="icon-text"/>',
                    '<div>No items available.</div>',
                '</div>',
            '</tpl>')
    }
})
Ext.define('ProductManager.view.Actions', {
    extend: 'Ext.ActionSheet',
    xtype: 'actions',
    config: {
        items: [
            {
                text: 'Delete',
                ui: 'confirm',
                handler: function () {
                    var me = this.up('actions');

                    me.fireButtonPressedEvent('delete', me);
                }
            },
            {
                text: 'Cancel',
                ui: 'decline',
                handler: function () {
                    var me = this.up('actions');

                    me.fireButtonPressedEvent('cancel', me);
                }
            }]
    },

    fireButtonPressedEvent: function (action, actionsheet) {
        actionsheet.fireEvent("buttonpressed", action, actionsheet);
    }
});
Ext.define('ProductManager.view.Main', {
    extend: 'Ext.navigation.View',
    xtype: 'main',
    requires: ['ProductManager.view.CategoriesList'],
    config: {
        items: [{
            xtype: 'categories'
        }],

        navigationBar: {
            hidden: true,
            name: 'navigationbackbutton'
        }
    }
});
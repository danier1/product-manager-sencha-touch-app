Ext.define('ProductManager.view.Products', {
    extend: 'Ext.Carousel',
    xtype: 'products',

    config: {
        baseCls: 'products-carousel',

        direction: 'horizontal',

        indicator: false,

        innerItemConfig: {
            xclass: 'ProductManager.view.ProductDetail'
        },

        listeners: {
            activeitemchange: 'onActiveItemChange'
        },
        items: [{
            xtype: 'toolbar',
            docked: 'top',
            width: '100%',
            right: 0,
            items: [{
                docked: 'right',
                iconCls: 'trash',
                ui: 'round',
                itemId: 'removeProductButton'
            }, {
                docked: 'right',
                iconCls: 'compose',
                ui: 'round',
                itemId: 'editProductButton'
            }]
        }, {
            xtype: 'component'
        }, {
            xtype: 'productdetail',
            data: {}
        }, {
            xtype: 'component'
        }],

        currentRecord: null,

        store: null
    },

    onActiveItemChange: function (carousel) {
        var me = this,
            store = me.getStore(),
            page = store.currentPage,
            storeCount = store.getTotalCount(),
            lastPage = Math.ceil(storeCount / store.getPageSize());

        var direction = me.animationDirection;

        if (!store.isLoading()) {
            switch (direction) {
                case 1:
                    if (page > 1) {
                        page--;
                    } else {
                        page = lastPage;
                    }
                    break;
                case -1:
                    if (page < lastPage) {
                        page++;
                    } else {
                        page = 1;
                    }
                    break;
                default:
                    page = 1;
                    break;
            }

            store.loadPage(page);
        }

        me.setMasked({
            xtype: 'loadmask'
        });

        this.setActiveItem(this.down('productdetail'));
    },

    initialize: function () {
        var el = this.element;

        el.on(['touchstart', 'touchend'], function (e) {
            this.fireEvent('touchaction', e);
        }, this);
    },

    applyStore: function (store) {
        return store;
    },

    updateStore: function (newStore) {
        var me = this;

        if (newStore.isLoading()) {
            me.setMasked({
                xtype: 'loadmask'
            });

            newStore.on('load', function () {
                me.setMasked(false);

                me.updateStore(newStore);
            }, me, {
                single: true
            });
        } else {
            newStore.on('load', me.onStoreLoad, me);

            newStore.setPageSize(1);

            newStore.currentPage = 0;

            me.setActiveItem(me.down('productdetail'));
        }
    },

    onStoreLoad: function (st, records) {
        var me = this,
            detail = me.down('productdetail');

        if (records.length > 0) {
            var data = records[0].data;

            me.setCurrentRecord(data);

            data.CurrentPage = st.currentPage;

            data.TotalPages = st.getTotalCount();

            detail.setData(data);

            me.setToolbarVisibility(false);

        } else {
            detail.setData({});

            me.setToolbarVisibility(true);
        }

        me.setMasked(false);
    },

    refreshIfLoaded: function () {
        var store = this.getStore();

        if (store && !store.isLoading()) {
            store.loadPage(1);
        }
    },

    showLastPage: function () {
        var store = this.getStore(),
            lastPage = 1;

        if (store) lastPage = store.currentPage;

        store.loadPage(lastPage);
    },

    showPreviousPage: function () {
        var store = this.getStore(),
            lastPage,
            page;


        if (store) {
            lastPage = store.currentPage;

            page = lastPage - 1;

            if (page < 1) throw new Error("Wrong page.");

            store.loadPage(page);
        }
    },

    setToolbarVisibility: function (visible) {
        var toolbar = this.down('toolbar');

        toolbar.setHidden(visible);
    }
});
Ext.define('ProductManager.view.ImageActions', {
    extend: 'Ext.ActionSheet',

    xtype: 'imageactions',

    config: {
        items: [
            {
                text: 'Select from library',
                ui: 'confirm',
                handler: function () {
                    var me = this.up('imageactions');

                    me.fireButtonPressedEvent('selectpicture', me);
                }
            },
            {
                text: 'Take picture',
                ui: 'confirm',
                handler: function () {
                    var me = this.up('imageactions');

                    me.fireButtonPressedEvent('takepicture', me);
                }
            }, {
                text: 'Cancel',
                ui: 'decline',
                handler: function () {
                    var me = this.up('imageactions');

                    Ext.Viewport.remove(me, true);
                }
            }]
    },

    fireButtonPressedEvent: function (action, actionsheet) {
        actionsheet.fireEvent("buttonpressed", action, actionsheet);
    }
});
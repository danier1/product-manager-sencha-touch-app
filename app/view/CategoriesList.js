Ext.define('ProductManager.view.CategoriesList', {
    extend: 'Ext.dataview.NestedList',
    xtype: 'categories',
    id: 'catlist',
    requires: ['ProductManager.view.Products'],

    config: {
        baseCls: 'categories-list',
        title: 'Categories',
        store: 'Categories',
        displayField: 'Name',
        useTitleAsBackText: false,
        backText: '',

        items: [{
            xtype: 'toolbar',
            docked: 'bottom',
            items: [{
                iconCls: 'add',
                name: 'buttonaddelement',
                ui: 'plain'
            },
                {
                    iconCls: 'compose',
                    ui: 'plain',
                    name: 'buttoneditcategory',
                    hidden: true
                }, {
                    iconCls: 'trash',
                    ui: 'plain',
                    name: 'buttondeletecategory',
                    hidden: true
                }, {
                    xtype: 'spacer'
                }, {
                    iconCls: 'refresh',
                    ui: 'plain',
                    name: 'buttonrefresh',
                }, {
                    iconCls: 'settings',
                    ui: 'plain',
                    name: 'buttonsettings',
                }]
        }],
        toolbar: {
            titleAlign: 'left'
        },
        detailCard: {
            xtype: 'products'
        },
        backButton: {
            ui: 'plain',
            iconCls: 'arrow_left',
            name: 'backbutton'
        }
    },

    onItemTap: function (list, index, target, record, e) {
        if (e.target.localName == 'a') {
            return;
        }

        this.callParent(arguments);
    },
    
    getItemTextTpl: function (rec) {
        return [
            '<img class="image" src="{url}"/>',
            '<span>',
            '<h1 class="name">{Name}</h1>',
            '<h1 class="description">{Description}</h1>',
            '</span>',
            '<div class="actions">',
            '<a href="#editcategory/{id}" class="link-edit"></a>',
            '<a href="#removecategory/{id}" class="link-remove"></a>',
            '</div>'
        ].join('');
    }
});
Ext.define('ProductManager.controller.Category', {
    extend: 'ProductManager.controller.Main',

    config: {
        stores: [
            'ProductManager.store.Categories'
        ],

        refs: {
            categorySaveButton: 'toolbar > #saveCategoryButton'
        },

        control: {
            categorySaveButton: {
                tap: 'onCategorySaveButtonTap'
            },

            buttonEditCategory: {
                tap: 'onButtonEditCategory'
            },

            buttonDeleteCategory: {
                tap: 'onButtonDeleteCategory'
            },

            editImage: {
                tap: 'onEditImageTap'
            },

            categoriesList: {
                leafitemtap: 'tryLoadProducts'
            },

            'toolbar > button[name=buttonrefresh]': {
                tap: 'reloadList'
            }
        },

        routes: {
            'editcategory/:id': 'editElement',
            'removecategory/:id': 'removeCategory'
        },
    },

    onCategorySaveButtonTap: function () {
        var me = this,
            editView = me.getEditView(),
            stack = me.getStack(),
            length = stack.length,
            data = editView.getValues(),
            category = Ext.create('ProductManager.model.Category', data),
            list = me.getCategoriesList(),
            record = list.getStore().getRoot(),
            errors = null;

        if (length > 0) {
            record = stack[length - 1];
            category.set('ParentCategoryId', record.data.CategoryId);
        }

        errors = category.validate();

        if (!errors.isValid()) {
            this.showValidationErrors(errors.items);

            return;
        }

        me.showLoadMask(true, 'Saving category...');

        category.save({
            success: function (rec, act) {
                var responseText = Ext.decode(act._response.responseText);

                me.showLoadMask(false);

                if (editView.shouldUpdateImage()) {
                    me.saveCategoryImage(category, record, responseText);
                } else {
                    me.executePostSaveTasks(category, record, responseText);
                }

            },
            failure: function (rec, response) {
                me.showLoadMask(false);

                if (response.error) {
                    var statusCode = response.error.status;

                    switch (statusCode) {
                        /*Concurrency error*/
                        case 409:
                            me.askToReloadCategoryForm(rec.data.CategoryId, editView);
                            break;
                        default:
                            me.showNotification("There have been some errors while saving this category.", 'Error');
                            break;
                    }
                }
            }
        });
    },

    saveCategoryImage: function (category, record, response) {

        var me = this,
            fileUri = this.getEditImage().getSrc(),
            imageUploader = Ext.create('ProductManager.util.ImageUploader'),
            extraParams = new Object();

        extraParams.categoryId = response.Id;

        imageUploader.upload(
            fileUri,
            ProductManager.app.getServerUrl() + "/api/CategoryImages",
            extraParams,
            function () {
                me.executePostSaveTasks(category, record, response);
            }, function () {
                me.showNotification("The category's picture haven't been saved correctly.", "Error");

                me.executePostSaveTasks(category, record, response);
            });
    },

    executePostSaveTasks: function (category, record, response) {
        var main = this.getMain(),
            list = this.getCategoriesList();

        main.pop();

        main.getNavigationBar().hide();

        this.navigateToUrl('/');

        if (!category.data.CategoryId) record.removeChild(category);

        category.data.CategoryId = category.data.id = response.Id;

        category.data.RowVersion = response.RowVersion;

        category.data.leaf = response.ChildrenType;

        var newCategory = Ext.create('ProductManager.model.Category', category.data);

        record.insertChild(record.childNodes.length - 1, newCategory);

        list.goToNode(record);
    },

    onButtonEditCategory: function () {
        var stack = this.getStack(),
            length = stack.length,
            record = stack[length - 1],
            id = record.data.CategoryId;

        this.editElement(id);
    },

    tryLoadProducts: function (nestedList, list, index, target, record) {
        var categoryId = record.data.CategoryId,
            store = record.products(),
            carousel = this.getCarousel();

        store.getProxy().setExtraParam('categoryId', categoryId);

        var carouselStore = carousel.getStore();

        if (carouselStore) {
            carouselStore.removeAll();
        }

        carousel.setStore(store);

        carousel.refreshIfLoaded();
    },

    editElement: function (id) {
        if (!this.checkIfShouldDisplayCategoryEditView(id)) return;

        var me = this,
            main = me.getMain(),
            list = me.getCategoriesList(),
            store = list.getStore(),
            record = store.findRecord('id', id),
            editview = Ext.create('ProductManager.form.category.Edit');

        main.getNavigationBar().show();

        main.push(editview);

        me.setEditView(editview);

        if (record) editview.setRecord(record);
    },

    onButtonDeleteCategory: function () {
        var stack = this.getStack(),
            length = stack.length,
            record = stack[length - 1],
            id = record.data.CategoryId;

        this.removeCategory(id);
    },

    removeCategory: function (id) {
        var store = Ext.getStore("Categories"),
            record = store.findRecord('id', id),
            list = this.getCategoriesList(),
            parent = null;

        var category = Ext.create('ProductManager.model.Category', record.data);

        this.eraseModel(category, function () {
            parent = record.parentNode;

            parent.removeChild(record, true);

            list.goToNode(parent);
        });

        this.navigateToUrl('/');
    },

    askToReloadCategoryForm: function (id, editView) {
        var me = this;

        this.showNotification('This element has been changed by another application. Click OK to reload from database.', 'Confirmation', me.reloadCategoryEditView(id, editView));
    },

    reloadCategoryEditView: function (id, editView) {
        var category = Ext.ModelMgr.getModel('ProductManager.model.Category');

        category.load(id, {
            success: function (categoryRecord, action) {
                var responseRec = Ext.decode(action._response.responseText),
                    record = Ext.create('ProductManager.model.Category', responseRec);

                record.set('id', responseRec.Id);

                if (record) editView.setRecord(record);
            }
        });
    },

    reloadList: function () {
        var list = this.getCategoriesList(),
            store = list.getStore(),
            root = store.getRoot();


        list.goToNode(root);

        store.load(root);
    }
});
Ext.define('ProductManager.controller.Product', {
    extend: 'ProductManager.controller.Main',

    config: {
        refs: {
            productSaveButton: 'toolbar > #saveProductButton'
        },
        control: {
            editProductButton: {
                tap: 'onEditProductButtonTap'
            },

            productSaveButton: {
                tap: 'onProductSaveButtonTap'
            },

            removeProductButton: {
                tap: 'removeProduct'
            },
        }
    },

    onProductSaveButtonTap: function () {
        var me = this,
            main = me.getMain(),
            editView = me.getEditView(),
            stack = me.getStack(),
            length = stack.length,
            data = editView.getValues(),
            product = Ext.create('ProductManager.model.Product', data),
            errors = null;

        if (length > 0) {
            product.set('CategoryId', stack[length - 1].data.CategoryId);
        }

        errors = product.validate();

        if (!errors.isValid()) {
            this.showValidationErrors(errors.items);

            return;
        }

        me.showLoadMask(true, 'Saving product...');

        product.save({
            success: function (rec, act) {

                var responseText = Ext.decode(act._response.responseText);

                me.showLoadMask(false);

                if (editView.shouldUpdateImage()) {
                    me.saveProductImage(responseText);
                } else {
                    me.executePostSaveTasks();
                }
            },

            failure: function (rec, response) {
                if (response.error) {
                    var statusCode = response.error.status;

                    switch (statusCode) {
                        /*Concurrency error*/
                        case 409:
                            me.askToReloadProductForm(rec.data.ProductId, editView);
                            break;
                        default:
                    }
                }

                me.showLoadMask(false);
            }
        });
    },

    saveProductImage: function (response) {

        var me = this,
            fileUri = this.getEditImage().getSrc(),
            imageUploader = Ext.create('ProductManager.util.ImageUploader'),
            extraParams = new Object();

        extraParams.productId = response.Id;

        imageUploader.upload(
            fileUri,
            "http://192.168.1.100/services/api/ProductImages",
            extraParams,
            function () {
                me.executePostSaveTasks();
            }, function () {
                me.showNotification("The product's picture haven't been saved correctly.", "Error");

                me.executePostSaveTasks();
            });
    },

    executePostSaveTasks: function () {
        var main = this.getMain(),
            carousel = this.getCarousel();

        main.pop();

        this.navigateToUrl('/');

        main.getNavigationBar().hide();

        carousel.showLastPage();
    },

    askToReloadProductForm: function (id, editView) {
        var me = this;

        Ext.device.Notification.show({
            title: 'Confirmation',
            message: 'This element has been changed by another application. Click OK to reload from database.',
            buttons: Ext.MessageBox.OK,
            callback: function () {
                me.reloadProductEditView(id, editView);
            }
        });
    },

    reloadProductEditView: function (id, editView) {
        var product = Ext.ModelMgr.getModel('ProductManager.model.Product');

        product.load(id, {
            success: function (categoryRecord, action) {
                var record = Ext.create('ProductManager.model.Product', Ext.decode(action._response.responseText));

                if (record) editView.setRecord(record);
            }
        });
    },

    onEditProductButtonTap: function () {
        this.editElement(true);
    },

    editElement: function (editing) {
        if (this.checkIfShouldDisplayCategoryEditView()) return;

        var main = this.getMain(),
            carousel = this.getCarousel(),
            detailContainer = carousel.getActiveItem(),
            data = editing ? detailContainer.getData() : Ext.emptyObj,
            product,
            editview = Ext.create('ProductManager.form.product.Edit');

        product = Ext.create("ProductManager.model.Product", data);

        main.getNavigationBar().show();

        main.push(editview);

        editview.setRecord(product);

        this.setEditView(editview);
    },

    removeProduct: function () {
        var me = this,
            carousel = this.getCarousel(),
            record = carousel.getCurrentRecord(),
            product = Ext.create('ProductManager.model.Product', record);

        if (product && product.data.ProductId) {
            this.eraseModel(product, function () {
                try {
                    carousel.showPreviousPage();
                } catch (e) {
                    me.navigateToLastVisitedNode();
                }
            });
        }
    }
});
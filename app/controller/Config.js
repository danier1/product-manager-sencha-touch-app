Ext.define('ProductManager.controller.Config', {
        extend: 'Ext.app.Controller',

        requires: ['ProductManager.form.Config'],

        config: {
            refs: {
                main: 'main',
                urlField: 'urlfield[name=url]'
            },
            control: {
                'toolbar > button[name=buttonsettings]': {
                    tap: 'onButtonSettingsTap'
                },

                'toolbar > button[name=configcancel]': {
                    tap: 'onButtonSettingsCancelTap'
                },

                'toolbar > button[name=configsave]': {
                    tap: 'onButtonSettingsSaveTap'
                }
            }
        },

        init: function() {
            document.addEventListener("backbutton", this.onBackButtonPressed, false);
        },

        onBackButtonPressed: function() {
            var mainController = ProductManager.app.getController('Category'),
                stack;

            if (!mainController) return;

            stack = mainController.getStack();

            if (stack.length > 0) {
                mainController.navigateToLastVisitedNode(true);
            } else {
                navigator.app.exitApp();
            }
        },

        onButtonSettingsTap: function() {
            var main = this.getMain(),
                configForm = Ext.create('ProductManager.form.Config'),
                urlField = this.getUrlField();

            urlField.setValue(ProductManager.app.getServerUrl());

            main.push(configForm);
        },

        onButtonSettingsCancelTap: function() {
            var main = this.getMain();

            main.pop();
        },

        onButtonSettingsSaveTap: function() {
            var main = this.getMain(),
                configStore = Ext.getStore('Config'),
                url = this.getUrlField().getValue();

            if (Ext.String.trim(url) == '') {
                Ext.device.Notification.show({
                    title: 'Error',
                    message: "Server Url field is mandatory.",
                    buttons: Ext.MessageBox.OK
                });
            } else {
                configStore.load({
                    callback: function (records) {
                        if (records.length > 0) {
                            var record = records[0];

                            record.set('server', url);

                            configStore.sync();

                            Ext.device.Notification.show({
                                title: 'Info',
                                message: "We'll have to reload the application for the changes to take effect.",
                                buttons: Ext.MessageBox.OK,
                                callback: function() {
                                    location.reload();
                                }
                            });
                        }
                    }
                });
            }
        }
    }
);
Ext.define('ProductManager.controller.Main', {
    extend: 'Ext.app.Controller',

    requires: [
        'ProductManager.view.ProductDetail',
        'ProductManager.form.product.Edit',
        'ProductManager.form.category.Edit',
        'ProductManager.view.Actions',
        'ProductManager.view.ImageActions',
        'ProductManager.util.ImageUploader'
    ],

    config: {
        refs: {
            main: 'main',
            categoriesList: 'categories',
            carousel: 'products',
            buttonAddElement: 'categories button[name=buttonaddelement]',
            buttonEditCategory: 'categories button[name=buttoneditcategory]',
            buttonDeleteCategory: 'categories button[name=buttondeletecategory]',
            backButton: 'button[name=backbutton]',
            editProductButton: 'toolbar > #editProductButton',
            removeProductButton: 'toolbar > #removeProductButton',
            editImage: 'image[name=imageEdit]'
        },

        control: {
            viewport: {
                orientationchange: 'onViewportOrientationChange'
            },

            categoriesList: {
                itemtap: 'onItemTap'
            },

            backButton: {
                tap: 'onBackButtonTap'
            },

            main: {
                back: 'onNavigationViewBackButtonTap'
            },

            carousel: {
                touchaction: 'onTouchAction'
            },

            buttonAddElement: {
                tap: 'onButtonAddElement'
            }
        },

        stack: [],

        corouselData: null,

        editView: null
    },

    onButtonAddElement: function () {
        this.editElement();
    },

    editElement: Ext.emptyFn,

    checkIfShouldDisplayCategoryEditView: function (val) {
        var me = this,
            stack = me.getStack(),
            lastRecord = stack[stack.length - 1],
            left = lastRecord && lastRecord.data.leaf;

        return val || !left;
    },

    chooseImage: function () {
        var actions = Ext.create('ProductManager.view.ImageActions');

        Ext.Viewport.add(actions);

        actions.show();

        actions.on('buttonpressed', 'processImageAction', this, { id: 1 });
    },

    processImageAction: function (action, actionsheet, options) {
        var me = this,
            source = (action === 'selectpicture') ? 'library' : 'camera';

        Ext.device.Camera.capture({
            success: function (image) {
                me.getEditImage().setSrc(image);
            },
            quality: 75,
            width: 200,
            height: 200,
            destination: 'file',
            source: source
        });


        Ext.Viewport.remove(actionsheet, true);
    },

    eraseModel: function (model, callback) {
        if (!model) return;

        var main = this.getMain();

        Ext.device.Notification.show({
            title: 'Confirmation',
            message: 'Are you sure you want to delete this element?',
            buttons: Ext.MessageBox.OKCANCEL,
            callback: function (button) {
                if (button === "ok") {
                    main.setMasked({
                        xtype: 'loadmask'
                    });

                    model.erase({
                        success: callback
                    });

                    main.setMasked(false);
                }
            }
        });
    },

    onTouchAction: function (e) {
        var editBtn = this.getEditProductButton(),
            removeBtn = this.getRemoveProductButton();

        if (e.type == 'touchstart') {
            editBtn.hide();
            removeBtn.hide();
        } else {
            editBtn.show(true);
            removeBtn.show(true);
        }
    },

    onItemTap: function (nestedlist, list, index, target, record) {
        this.getStack().push(record);

        this.setHiddenBottomBarButtons(false);
    },

    onBackButtonTap: function () {
        var stack = this.getStack(),
            length = stack.length,
            me = this;

        if (length > 0) {
            stack.pop();

            if (--length == 0) me.setHiddenBottomBarButtons(true);
        }
    },

    setHiddenBottomBarButtons: function (value) {
        var buttonEditCategory = this.getButtonEditCategory(),
            buttonDeleteCategory = this.getButtonDeleteCategory();

        if (buttonEditCategory.getHidden() != value) {
            buttonEditCategory.setHidden(value);
            buttonDeleteCategory.setHidden(value);
        }
    },

    onNavigationViewBackButtonTap: function () {
        var main = this.getMain();

        this.navigateToUrl('/');

        main.getNavigationBar().hide();
    },

    navigateToLastVisitedNode: function (deleteLastNode) {
        var stack = this.getStack(),
            length = stack.length,
            list = this.getCategoriesList(),
            node;

        if (length > 0) {
            node = stack[length - 1];

            list.goToNode(node.parentNode);

            if (deleteLastNode) stack.pop();
        }
    },

    navigateToUrl: function (url) {
        var history = this.getApplication().getHistory();

        history.add(new Ext.app.Action({
            url: url
        }), true);
    },

    onEditImageTap: function () {
        this.chooseImage();
    },

    showValidationErrors: function (errorItems) {
        var ulString = "<ul>";

        Ext.Array.each(errorItems, function (error) {
            ulString += "<li> Field " + error.getField() + " " + error.getMessage() + "<li>";
        });

        ulString += "</ul>";

        Ext.device.Notification.show({
            title: 'Errors',
            message: ulString,
            buttons: Ext.MessageBox.OK
        });
    },

    showLoadMask: function (show, msg) {
        var main = this.getMain();

        if (show) {
            main.setMasked({
                xtype: 'loadmask',
                message: msg ? msg : 'Loading...'
            });
        } else {
            main.setMasked(false);
        }
    },

    showNotification: function (msg, title, callback) {
        Ext.device.Notification.show({
            title: title || 'Info',
            message: msg,
            buttons: Ext.MessageBox.OK,
            callback: callback || Ext.emptyFn
        });
    }
});
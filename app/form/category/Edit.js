Ext.define('ProductManager.form.category.Edit', {
    extend: 'Ext.form.Panel',
    xtype: 'editcategory',
    requires: [
        'Ext.field.Text',
        'Ext.field.Radio',
        'Ext.field.Hidden',
        'Ext.field.File',
        'Ext.form.FieldSet',
        'Ext.Img'
    ],

    config: {
        title: 'Edit category',
        items: [{
            xtype: 'image',
            src: 'resources/images/thumbnail.jpg',
            height: 70,
            width: 70,
            margin: '15px auto -15px 15px',
            name: 'imageEdit',
            cls: 'image-edit'
        }, {
            xtype: 'fieldset',
            title: 'Data',
            defaults: {
                labelWidth: '45%'
            },
            items: [{
                name: 'id',
                xtype: 'hiddenfield'
            }, {
                name: 'CategoryId',
                xtype: 'hiddenfield'
            }, {
                name: 'RowVersion',
                xtype: 'hiddenfield'
            }, {
                name: 'ParentCategoryId',
                xtype: 'hiddenfield'
            }, {
                name: 'HasSubCategories',
                xtype: 'hiddenfield'
            }, {
                name: 'HasProducts',
                xtype: 'hiddenfield'
            }, {
                name: 'Name',
                label: 'Name',
                xtype: 'textfield'
            }, {
                name: 'Description',
                label: 'Description',
                xtype: 'textareafield'
            }]
        }, {
            xtype: 'fieldset',
            title: 'Contains',
            name: 'fsContains',
            defaults: {
                labelWidth: '45%',
                xtype: 'radiofield',
                name: 'ChildrenType'
            },
            items: [{
                value: '0',
                label: 'Categories'
            }, {
                value: '1',
                label: 'Products',
                checked: true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: { pack: 'center' },
            items: [{
                text: 'Save',
                itemId: 'saveCategoryButton'
            }, {
                text: 'Clear'
            }]
        }],

        imageSrc: null
    },

    setRecord: function (record) {
        this.callParent(arguments);

        if (record) {

            var radio = this.down('radiofield[value=0]'),
                type = record.get('ChildrenType'),
                containsFieldSet = this.down('fieldset[name=fsContains]'),
                image = this.down('image[name=imageEdit]'),
                disabled = (type === 0 && record.data.HasSubCategories) || (type === 1 && record.data.HasProducts) ? true : false,
                src = ProductManager.app.getServerUrl() + "/api/CategoryImages?id=" + record.data.CategoryId;

            if (type == 0) radio.check();

            containsFieldSet.setDisabled(disabled);

            image.setSrc(src);

            this.setImageSrc(src);
        }
    },

    shouldUpdateImage: function () {
        var image = this.down('image[name=imageEdit]');

        return image.getSrc() != this.getImageSrc();
    }
});
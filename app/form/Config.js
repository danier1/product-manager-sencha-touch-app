﻿Ext.define('ProductManager.form.Config', {
    extend: 'Ext.form.Panel',
    xtype: 'fields',

    requires: ['Ext.field.Url'],

    config: {
        title: 'name',

        items: [{
                xtype: 'toolbar',
                docked: 'bottom',
                layout: { pack: 'center' },
                items: [{
                        text: 'Cancel',
                        name: 'configcancel'
                    }, {
                        text: 'Save',
                        name: 'configsave'
                    }]
            }, {
                xtype: 'fieldset',
                title: 'Configuration',
                instructions: 'Enter configuration data',
                items: [{
                    xtype: 'urlfield',
                    labelWidth: 100,
                    name: 'url',
                    label: 'Server Url'
                }]
            }]
    }
});
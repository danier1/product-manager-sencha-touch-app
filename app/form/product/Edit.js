﻿Ext.define('ProductManager.form.product.Edit', {
    extend: 'Ext.form.Panel',
    xtype: 'editproduct',
    requires: [
        'Ext.field.Text',
        'Ext.field.Radio',
        'Ext.field.Hidden',
        'Ext.field.Number',
        'Ext.field.File',
        'Ext.form.FieldSet'
    ],
    config: {
        minHeight: 300,
        title: 'Edit product',
        items: [{
                xtype: 'image',
                src: 'resources/images/thumbnail.jpg',
                height: 70,
                width: 70,
                margin: '15px auto -15px 15px',
                name: 'imageEdit',
                cls: 'image-edit'
            }, {
                xtype: 'fieldset',
                title: 'Data',
                defaults: {
                    labelWidth: '45%'
                },
                items: [{
                        name: 'id',
                        xtype: 'hiddenfield'
                    }, {
                        name: 'RowVersion',
                        xtype: 'hiddenfield'
                    }, {
                        name: 'CategoryId',
                        xtype: 'hiddenfield'
                    }, {
                        name: 'ProductId',
                        xtype: 'hiddenfield'
                    }, {
                        name: 'Name',
                        label: 'Name',
                        xtype: 'textfield'
                    }, {
                        name: 'Price',
                        label: 'Price',
                        allowDecimals: true,
                        xtype: 'numberfield'
                    }, {
                        name: 'Quantity',
                        label: 'Quantity',
                        value: '1',
                        allowDecimals: false,
                        xtype: 'numberfield'
                    }, {
                        name: 'Description',
                        label: 'Description',
                        xtype: 'textareafield'
                    }]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: { pack: 'center' },
                items: [{
                        text: 'Save',
                        itemId: 'saveProductButton'
                    }, {
                        text: 'Clear'
                    }]
            }],
        
        imageSrc: null
    },
    
    setRecord: function (record) {
        this.callParent(arguments);

        if (record) {
            var src = ProductManager.app.getServerUrl() + "/api/ProductImages?id=" + record.data.ProductId,
                image = this.down('image[name=imageEdit]');

            image.setSrc(src);

            this.setImageSrc(src);
        }
    },

    shouldUpdateImage: function () {
        var image = this.down('image[name=imageEdit]');

        return image.getSrc() != this.getImageSrc();
    }
});